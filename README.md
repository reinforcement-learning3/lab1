# Lab 1 from [UC Berkeley Deep RL Bootcamp](https://sites.google.com/view/deep-rl-bootcamp/lectures)

## Lectures

- [Intro to MDPs and Exact Solution Methods](https://www.youtube.com/watch?v=qaMdN6LS9rA)
- [Sample-based Approximations and Fitted Learning](https://www.youtube.com/watch?v=qO-HUo0LsO4)