# Grading of Lab1

## Problem 1

### Positive

Instead of keeping track of the best_value_state (like I did) the value states are captured in an array and then the argmax is taken as visible below.

```python
 for state in range(mdp.nS):
            A_a = np.zeros(mdp.nA)
            for action in mdp.P[state]:
                for prob, next_state, reward in mdp.P[state][action]:
                    A_a[action] += prob * (reward + gamma * Vprev[next_state])
            argmx = np.argmax(A_a)
            pi[state] = argmx
            V[state] = A_a[argmx]
```

I think this is more elegant than my solution which tracks the highest value and exchanges it if necessary.

---

I also like the iteration below more than my own which was implemented with `dict.items()`, it is more explicit and understanding.

```python
for action in mdp.P[state]:
                for prob, next_state, reward in mdp.P[state][action]:
```

### Negative

- Some prints (cell5) and cells(4) could be removed.
- I prefer having more comments and documentation in the code since this is a learning assignment.

## Problem 2

### Positive

All tests pass and the notebook works as expected.

### Negative

- Some comments could be removed.

## Problem 3

### Positive

All tests pass and the notebook works as expected.

### Negative

Nothing

## Grading

Overall I would give a **5.25** since I was missing more comments and explanations of the code (might be my preference). Additionally some comments could have been removed but I dont regards this as anything major.

The notebook runs as expected and with the corrections above I would have given a 5.5 for very good work and completing the notebooks.
